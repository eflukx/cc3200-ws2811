//*****************************************************************************
// Stukkie code om een WS2812 LED string aan te sturen....
//*****************************************************************************

#include <string.h>
#include <stdlib.h>
#include "hw_types.h"
#include "hw_memmap.h"
#include "hw_common_reg.h"
#include "hw_ints.h"
#include "spi.h"
#include "pin.h"
#include "rom.h"
#include "rom_map.h"
#include "utils.h"
#include "prcm.h"
#include "interrupt.h"
#include "pinmux.h"
#include "uart.h"
#include "uart_if.h"

#include "hw_mcspi.h"
#include "hw_nvic.h"

// #define NVIC_ST_CTRL            0xE000E010  // SysTick Control and Status Register
// #define NVIC_ST_RELOAD          0xE000E014  // SysTick Reload Value Register
// #define NVIC_ST_CURRENT         0xE000E018

//*****************************************************************************
//
// Application Master/Slave mode selector macro
//
// MASTER_MODE = 1 : Application in master mode
// MASTER_MODE = 0 : Application in slave mode
//
//*****************************************************************************
#define MASTER_MODE      1

#define NUM_PIXELS		  10
#define WSSTRING_BYTES  NUM_PIXELS*24


#define SPI_IF_BIT_RATE  6000000
#define TR_BUFF_SIZE     300

#define WS_LOW  0xc0
#define WS_HIGH  0xf0

#define MASTER_MSG "This is CC3200 SPI Master Application\n\r"
#define SLAVE_MSG "This is CC3200 SPI Slave Application\n\r"

typedef unsigned char WSstream;

//pixels in encoded form for serial transmission over SPI to WS2811/12
typedef struct WSpixel{
  unsigned char r[8];
  unsigned char g[8];
  unsigned char b[8];
} WSpixel;

typedef struct WSstring {
    unsigned long length;
    WSpixel *WSpixels;
} WSstring;

typedef struct Pixel {
    unsigned char r;
    unsigned char g;
    unsigned char b;
} Pixel;

typedef struct LED_string {
    unsigned long length;
    Pixel *pixels;
} LED_string;

//*****************************************************************************
// Global variables
//*****************************************************************************
  static unsigned char g_ucTxBuff[TR_BUFF_SIZE], g_ucRxBuff[TR_BUFF_SIZE], g_ucTmpBuff[TR_BUFF_SIZE];
  unsigned long ulUserData = 0;
  unsigned int  string_length = NUM_PIXELS, x, i, index = 0;

#if defined(ccs)
extern void (* const g_pfnVectors[])(void);
#endif

#if defined(ewarm)
extern uVectorEntry __vector_table;
#endif
  
ASSERT_out_of_memory(line_nr){
  Report("KABOOEM.. Captain we've hit the memory boundary on line %i\n\rWe'll have to linger 'round here till the end of times... bye now.", line_nr);
  while(1){}
}

// ============================================================================
void rgbtoWSstream(WSstream *spibuf, int r, int g, int b){
  int i;
  for (i=0;i < 8; i++) spibuf[ 7 - i] = ((g >> i) & 1 == 1) ? WS_HIGH : WS_LOW;
  for (i=0;i < 8; i++) spibuf[15 - i] = ((r >> i) & 1 == 1) ? WS_HIGH : WS_LOW;
  for (i=0;i < 8; i++) spibuf[23 - i] = ((b >> i) & 1 == 1) ? WS_HIGH : WS_LOW;
}

void PixeltoWSstream(WSstream *spibuf, Pixel pixel){
  int i;
  for (i=0;i < 8; i++) spibuf[ 7 - i] = ((pixel.g >> i) & 1 == 1) ? WS_HIGH : WS_LOW;
  for (i=0;i < 8; i++) spibuf[15 - i] = ((pixel.r >> i) & 1 == 1) ? WS_HIGH : WS_LOW;
  for (i=0;i < 8; i++) spibuf[23 - i] = ((pixel.b >> i) & 1 == 1) ? WS_HIGH : WS_LOW;
}

void LED_stringtoWSstream(WSstream *spibuf, LED_string led_string){
  for (i = 0; i < led_string.length; i++) PixeltoWSstream(spibuf + i * 24, led_string.pixels[i]);
}
Pixel inttoPixel(unsigned int rgb){
	Pixel pixel;
  
	pixel.r = rgb >> 16 & 0xff;
	pixel.g = rgb >> 8  & 0xff;
	pixel.b = rgb       & 0xff;
	
  return(pixel);
}

Pixel rgbtoPixel(unsigned int r, unsigned int g, unsigned int b){
	Pixel pixel;
  
	pixel.r = r & 0xff;
	pixel.g = g & 0xff;
	pixel.b = b & 0xff;
	
  return(pixel);
}

// ============================================================================
void initSPIMaster(){

  MAP_PRCMPeripheralReset(PRCM_GSPI);
  
  MAP_PRCMPeripheralClkEnable(PRCM_GSPI,PRCM_RUN_MODE_CLK);

  MAP_SPIReset(GSPI_BASE);

  MAP_SPIConfigSetExpClk(GSPI_BASE,MAP_PRCMPeripheralClockGet(PRCM_GSPI),
                     SPI_IF_BIT_RATE, SPI_MODE_MASTER, SPI_SUB_MODE_0, 
                     SPI_SW_CTRL_CS | SPI_3PIN_MODE | SPI_TURBO_ON | SPI_CS_ACTIVEHIGH | SPI_WL_8);

  MAP_SPIEnable(GSPI_BASE);

  // Message("Enabled SPI Interface in Master Mode\n\r");
}

void display_LED_string(LED_string led_string){
  int i;
  WSstream WSbuffer[WSSTRING_BYTES];
  
  for (i = 0; i < led_string.length; i++) PixeltoWSstream(WSbuffer + i * 24, led_string.pixels[i]);
// 
    MAP_SPITransfer(GSPI_BASE,WSbuffer,g_ucRxBuff,WSSTRING_BYTES,SPI_CS_ENABLE|SPI_CS_DISABLE);
}

// ============================================================================
void MasterMain()
{
  initSPIMaster();
  
  LED_string my_led_string;
  
  my_led_string.pixels = malloc(string_length * sizeof(Pixel));
  if (my_led_string.pixels == NULL)
    ASSERT_out_of_memory(__LINE__);

  my_led_string.length = string_length;
 
  Report("Lengte LED string: %i (using %i bytes of memory)\n\r",my_led_string.length , my_led_string.length * sizeof(Pixel));

  
  for (x = 0; x < TR_BUFF_SIZE; x++) g_ucTxBuff[x] = 0;

  int increase = 20;//255 / string_length;
  int value = 20;
  for (i = 0; i < string_length; i++){
    my_led_string.pixels[i].r = value;
    my_led_string.pixels[i].g = 0;//value;
    my_led_string.pixels[i].b = 0;//255 - value;
    Report("Value: %i", value);
    
    value += increase;
  }
  
  //Alloceer twee*2 buffers grootte LED strip, type WSstring en LED_string
  
//scale: linear and NN
//display: LED_string type

  display_LED_string(my_led_string);

  index = 0;
  int buffersize = NUM_PIXELS * 24;

  
  while(ulUserData != 'a')
  {
  
    for (x=0;x < 256; x++){
      g_ucTmpBuff[x] = g_ucTxBuff[(x + index * 24 ) % buffersize];
    }
    
    // for (i = 0; i < string_length; i++){
      // rgbtoWSstream(g_ucTxBuff + i * 24, 0,index & 0xff,0 );
    // }
    
    MAP_SPITransfer(GSPI_BASE,g_ucTmpBuff,g_ucRxBuff,240,SPI_CS_ENABLE|SPI_CS_DISABLE);
    
    ulUserData = MAP_UARTCharGet(UARTA0_BASE);
    
    if (ulUserData == 'd'){
      --index;
    }else{
      ++index;
    } 

    Report("%i %c %i|", index, ulUserData, (index *24)% buffersize);
    // MAP_UARTCharPut(UARTA0_BASE,ulUserData);
  }

  free(my_led_string.pixels);

  Report("\n\rExiting master mode!");

  // MAP_SPICSDisable(GSPI_BASE);
}

//*****************************************************************************
static void BoardInit(void){
/* In case of TI-RTOS vector table is initialize by OS itself */
#ifndef USE_TIRTOS
  //
  // Set vector table base
  //
#if defined(ccs)
    MAP_IntVTableBaseSet((unsigned long)&g_pfnVectors[0]);
#endif
#if defined(ewarm)
    MAP_IntVTableBaseSet((unsigned long)&__vector_table);
#endif
#endif
  //
  // Enable Processor
  //
  MAP_IntMasterEnable();
  MAP_IntEnable(FAULT_SYSTICK);

  PRCMCC3200MCUInit();
  
  PinMuxConfig();
}

//*****************************************************************************
void main(){

  BoardInit();

  InitTerm();

  ClearTerm();

  Message("\n\n\n\r");
  Message("\t\t   ********************************************\n\r");
  Message("\t\t        CC3200 SPI Demo Application  \n\r");
  Message("\t\t   ********************************************\n\r");
  Message("\n\n\n\r");

#if MASTER_MODE
  MasterMain();
#else
  // SlaveMain();
#endif

  Report("\n\rExiting progam... bye...");
  while(1){}
}
